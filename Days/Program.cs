﻿namespace Days
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            BaseDay day = new Day15();
            day.Solve();
        }
    }
}