﻿namespace Days;

internal class Day03 : BaseDay
{
    public Day03()
    {
        SampleInputPartOne.Add(@">", "2");
        SampleInputPartOne.Add(@"^>v<", "4");
        SampleInputPartOne.Add(@"^v^v^v^v^v", "2");

        SampleInputPartTwo.Add(@"^v", "3");
        SampleInputPartTwo.Add(@"^>v<", "3");
        SampleInputPartTwo.Add(@"^v^v^v^v^v", "11");
    }

    protected override string FindFirstSolution(string[] input)
    {
        int solution = CountDistinctHouses(input[0]);

        return solution.ToString();
    }

    private int CountDistinctHouses(string input)
    {
        HashSet<(int, int)> houses = new();
        houses.Add((0, 0));

        int x = 0, y = 0;
        foreach (char direction in input)
        {
            (x, y) = GetNewLocation(x, y, direction);
            houses.Add((x, y));
        }

        return houses.Distinct().Count();
    }

    private (int, int) GetNewLocation(int x, int y, char direction)
    {
        x = direction switch
        {
            '<' => x - 1,
            '>' => x + 1,
            _ => x
        };

        y = direction switch
        {
            'v' => y - 1,
            '^' => y + 1,
            _ => y
        };

        return (x, y);
    }

    protected override string FindSecondSolution(string[] input)
    {
        int solution = CountDistinctHousesWithRobot(input[0]);

        return solution.ToString();
    }

    private int CountDistinctHousesWithRobot(string input)
    {
        HashSet<(int, int)> houses = new();
        houses.Add((0, 0));

        int xSanta = 0, ySanta = 0, xRobot = 0, yRobot = 0;
        
        for (int i = 0; i < input.Length; i++)
        {
            if (i % 2 == 0)
            {
                (xSanta, ySanta) = GetNewLocation(xSanta, ySanta, input[i]);
                houses.Add((xSanta, ySanta));
            }
            else
            {
                (xRobot, yRobot) = GetNewLocation(xRobot, yRobot, input[i]);
                houses.Add((xRobot, yRobot));
            }
        }

        return houses.Count;
    }
}
