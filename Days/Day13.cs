﻿using System.Text.RegularExpressions;

namespace Days
{
    internal class Day13 : BaseDay
    {
        public Day13()
        {
            SampleInputPartOne.Add(@"Alice would gain 54 happiness units by sitting next to Bob.
Alice would lose 79 happiness units by sitting next to Carol.
Alice would lose 2 happiness units by sitting next to David.
Bob would gain 83 happiness units by sitting next to Alice.
Bob would lose 7 happiness units by sitting next to Carol.
Bob would lose 63 happiness units by sitting next to David.
Carol would lose 62 happiness units by sitting next to Alice.
Carol would gain 60 happiness units by sitting next to Bob.
Carol would gain 55 happiness units by sitting next to David.
David would gain 46 happiness units by sitting next to Alice.
David would lose 7 happiness units by sitting next to Bob.
David would gain 41 happiness units by sitting next to Carol.", "330");

            SampleInputPartTwo.Add(@"", "");
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<(string, string), int> happinessPairs = ParseHappinessPairsFromInput(input);

            int sum = FindPerfectCombination(happinessPairs);

            return sum.ToString();
        }

        private int FindPerfectCombination(Dictionary<(string, string), int> happinessPairs)
        {
            string firstPerson = happinessPairs.Keys.First().Item1;
            int numberOfPersons = happinessPairs.Where(h => h.Key.Item1 == firstPerson).Count() + 1;

            Queue<(List<string> sitting, int happiness)> queue = new();
            queue.Enqueue((new() { firstPerson }, 0));

            int maxHappiness = 0;
            while (queue.Count > 0)
            {
                var item = queue.Dequeue();

                //if all persons are added to the table, we only have to link last and first person
                if (item.sitting.Count == numberOfPersons)
                {
                    int currentHappiness = item.happiness + happinessPairs[(item.sitting[item.sitting.Count - 1], firstPerson)];
                    if (currentHappiness > maxHappiness)
                        maxHappiness = currentHappiness;
                }

                //add remaing persons - all combinations
                foreach (var pair in happinessPairs.Where(h => h.Key.Item1 == item.sitting[item.sitting.Count - 1]))
                {
                    if (item.sitting.Contains(pair.Key.Item2)) continue;

                    //make new list with previous persons + one new one
                    List<string> list = [.. item.sitting, pair.Key.Item2];

                    queue.Enqueue((list, item.happiness + happinessPairs[pair.Key]));
                }
            }

            return maxHappiness;
        }

        private Dictionary<(string, string), int> ParseHappinessPairsFromInput(string[] input)
        {
            Dictionary<(string, string), int> happinessPairs = new();
            Regex regex = new Regex("(?<personOne>.+) would (?<operation>.+) (?<amount>\\d+) happiness units by sitting next to (?<personTwo>.+).");
            foreach (var line in input)
            {
                var match = regex.Match(line);
                happinessPairs.Add((match.Groups["personOne"].Value, match.Groups["personTwo"].Value), int.Parse(match.Groups["amount"].Value) * (match.Groups["operation"].Value == "gain" ? 1 : -1));
                if (happinessPairs.ContainsKey((match.Groups["personTwo"].Value, match.Groups["personOne"].Value)))
                {
                    happinessPairs[(match.Groups["personOne"].Value, match.Groups["personTwo"].Value)] += happinessPairs[(match.Groups["personTwo"].Value, match.Groups["personOne"].Value)];
                    happinessPairs[(match.Groups["personTwo"].Value, match.Groups["personOne"].Value)] += int.Parse(match.Groups["amount"].Value) * (match.Groups["operation"].Value == "gain" ? 1 : -1);
                }
            }

            return happinessPairs;
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<(string, string), int> happinessPairs = ParseHappinessPairsFromInput(input);

            AddMeToTheTable(happinessPairs);
            int sum = FindPerfectCombination(happinessPairs);

            return sum.ToString();
        }

        private void AddMeToTheTable(Dictionary<(string, string), int> happinessPairs)
        {
            List<string> persons = happinessPairs.Keys.Select(k => k.Item1).Distinct().ToList();
            foreach (string person in persons)
            {
                happinessPairs.Add((person, "Me"), 0);
                happinessPairs.Add(("Me", person), 0);
            }
        }
    }
}