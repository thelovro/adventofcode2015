﻿using System.Text.Json.Nodes;
using System.Text.RegularExpressions;

namespace Days
{
    internal class Day12 : BaseDay
    {
        public Day12()
        {
            SampleInputPartOne.Add(@"[1,2,3]", "6");
            SampleInputPartOne.Add(@"{""a"":2,""b"":4}", "6");
            SampleInputPartOne.Add(@"[[[3]]]", "3");
            SampleInputPartOne.Add(@"{""a"":{""b"":4},""c"":-1}", "3");
            SampleInputPartOne.Add(@"{""a"":[-1,1]}", "0");
            SampleInputPartOne.Add(@"[-1,{""a"":1}]", "0");
            SampleInputPartOne.Add(@"[]", "0");
            SampleInputPartOne.Add(@"{}", "0");

            SampleInputPartTwo.Add(@"[1,2,3]", "6");
            SampleInputPartTwo.Add(@"[1,{""c"":""red"",""b"":2},3]", "4");
            SampleInputPartTwo.Add(@"{""d"":""red"",""e"":[1,2,3,4],""f"":5}", "0");
            SampleInputPartTwo.Add(@"[1,""red"",5]", "6");
        }

        protected override string FindFirstSolution(string[] input)
        {
            Regex regex = new Regex("(-?\\d+)");
            long sum = regex.Matches(input[0]).Sum(m => int.Parse(m.Value));

            return sum.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            JsonNode? jsonObject = JsonNode.Parse(input[0]);

            int sum = SumItemsForObject(jsonObject);

            return sum.ToString();
        }

        private int SumItemsForObject(JsonNode? jsonObject)
        {
            int sum = 0;
            
            if (jsonObject is JsonArray)
            {
                foreach (var item in jsonObject.AsArray())
                {
                    int value;
                    if (item is JsonValue && int.TryParse(item.ToString(), out value))
                    {
                        sum += value;
                    }
                    else if (item is JsonObject || item is JsonArray)
                    {
                        sum += SumItemsForObject(item);
                    }
                }
            }
            else if (jsonObject is JsonObject)
            {
                foreach (var item in jsonObject.AsObject())
                {
                    int value;
                    if (item.Value is JsonValue && int.TryParse(item.Value.ToString(), out value))
                    {
                        sum += value;
                    }
                    else if (item.Value is JsonObject || item.Value is JsonArray)
                    {
                        sum += SumItemsForObject(item.Value);
                    }
                    else if (item.Value is JsonValue && item.Value.ToString() == "red")
                    {
                        //if one of values in Json object is "red", we mustn't sum the numbers of this object
                        return 0;
                    }
                }
            }

            return sum;
        }
    }
}