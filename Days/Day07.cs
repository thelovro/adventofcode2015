﻿namespace Days
{
    internal class Day07 : BaseDay
    {
        public Day07()
        {
            SampleInputPartOne.Add(@"123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i", "65412");

            SampleInputPartTwo.Add(@"", "");
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<string, int> wires = new();
            RunOperations(input, wires);

            return (IsTest ? wires["h"] : wires["a"]).ToString();
        }

        private static void RunOperations(string[] input, Dictionary<string, int> wires)
        {
            while (wires.Count < input.Length)
            {
                foreach (var line in input)
                {
                    string wire = line.Split(" -> ")[1];
                    if (wires.ContainsKey(wire)) continue;

                    string operation = line.Split(" -> ")[0];
                    
                    //direct assign
                    if (!operation.Contains(' '))
                    {
                        if (int.TryParse(operation, out _))
                            wires.Add(wire, int.Parse(operation));
                        else
                        {
                            if (!wires.ContainsKey(operation)) continue;
                            wires.Add(wire, wires[operation]);
                        }
                    }
                    //operation
                    else
                    {
                        //only one operator
                        if (operation.Contains("NOT"))
                        {
                            string inputWire = line.Split(" ")[1];
                            if (!wires.ContainsKey(inputWire)) continue;

                            wires.Add(wire, ~wires[inputWire] & 0xFFFF);
                        }
                        else
                        {
                            //check the input wires/constants
                            string inputWireOne = line.Split(" ")[0];
                            string inputWireTwo = line.Split(" ")[2];

                            int inputOne;
                            if (!int.TryParse(inputWireOne, out inputOne))
                            {
                                if (!wires.ContainsKey(inputWireOne)) continue;

                                inputOne = wires[inputWireOne];
                            }
                            int inputTwo;
                            if (!int.TryParse(inputWireTwo, out inputTwo))
                            {
                                if (!wires.ContainsKey(inputWireTwo)) continue;

                                inputTwo = wires[inputWireTwo];
                            }

                            //run the operation
                            if (operation.Contains("AND"))
                            {
                                wires.Add(wire, inputOne & inputTwo);
                            }
                            else if (operation.Contains("OR"))
                            {
                                wires.Add(wire, inputOne | inputTwo);
                            }
                            else if (operation.Contains("LSHIFT"))
                            {
                                wires.Add(wire, inputOne << inputTwo);
                            }
                            else if (operation.Contains("RSHIFT"))
                            {
                                wires.Add(wire, inputOne >> inputTwo);
                            }
                        }
                    }
                }
            }
        }

        protected override string FindSecondSolution(string[] input)
        {
            if (IsTest) { return "no test"; }

            Dictionary<string, int> wires = new();
            RunOperations(input, wires);
            int intermediateResult = wires["a"];
            wires = new();
            wires.Add("b", intermediateResult);

            RunOperations(input, wires);

            return (IsTest ? wires["h"] : wires["a"]).ToString();
        }
    }
}