﻿using System.Text.RegularExpressions;

namespace Days;

internal class Day06 : BaseDay
{
    public Day06()
    {
        SampleInputPartOne.Add(@"turn on 0,0 through 999,999", "1000000");
        SampleInputPartOne.Add(@"toggle 0,0 through 999,0", "1000");

        SampleInputPartTwo.Add(@"toggle 0,0 through 999,999", "2000000");
        SampleInputPartTwo.Add(@"turn on 0,0 through 0,0", "1");
    }

    protected override string FindFirstSolution(string[] input)
    {
        Dictionary<(int, int), bool> map = new();
        
        Regex regex = new Regex(@"(?<operation>.+) (?<xFrom>\d+),(?<yFrom>\d+) .+ (?<xTo>\d+),(?<yTo>\d+)");
        foreach (var line in input)
        {
            var match = regex.Match(line);
            int xFrom = int.Parse(match.Groups["xFrom"].Value);
            int xTo = int.Parse(match.Groups["xTo"].Value);
            int yFrom = int.Parse(match.Groups["yFrom"].Value);
            int yTo = int.Parse(match.Groups["yTo"].Value);
            if (match.Groups["operation"].Value.Contains("on"))
            {
                for(int y=yFrom; y<=yTo; y++)
                {
                    for (int x = xFrom; x <= xTo; x++)
                    {
                        map[(y, x)] = true;
                    }
                }
            }
            else if (match.Groups["operation"].Value.Contains("off"))
            {
                for (int y = yFrom; y <= yTo; y++)
                {
                    for (int x = xFrom; x <= xTo; x++)
                    {
                        map[(y, x)] = false;
                    }
                }
            }
            else
            {
                for (int y = yFrom; y <= yTo; y++)
                {
                    for (int x = xFrom; x <= xTo; x++)
                    {
                        if(map.ContainsKey((y,x)))
                        {
                            map[(y, x)] = !map[(y, x)];
                        }
                        else
                        {
                            map[(y, x)] = true;
                        }
                    }
                }
            }
        }

        return map.Where(m => m.Value).Count().ToString();
    }

    protected override string FindSecondSolution(string[] input)
    {
        Dictionary<(int, int), int> map = new();

        Regex regex = new Regex(@"(?<operation>.+) (?<xFrom>\d+),(?<yFrom>\d+) .+ (?<xTo>\d+),(?<yTo>\d+)");
        foreach (var line in input)
        {
            var match = regex.Match(line);
            int xFrom = int.Parse(match.Groups["xFrom"].Value);
            int xTo = int.Parse(match.Groups["xTo"].Value);
            int yFrom = int.Parse(match.Groups["yFrom"].Value);
            int yTo = int.Parse(match.Groups["yTo"].Value);
            if (match.Groups["operation"].Value.Contains("on"))
            {
                for (int y = yFrom; y <= yTo; y++)
                {
                    for (int x = xFrom; x <= xTo; x++)
                    {
                        if (map.ContainsKey((y, x)))
                        {
                            map[(y, x)] += 1;
                        }
                        else
                        {
                            map[(y, x)] = 1;
                        }
                    }
                }
            }
            else if (match.Groups["operation"].Value.Contains("of"))
            {
                for (int y = yFrom; y <= yTo; y++)
                {
                    for (int x = xFrom; x <= xTo; x++)
                    {
                        if (map.ContainsKey((y, x)) && map[(y,x)] >=1)
                        {
                            map[(y, x)] -= 1;
                        }
                    }
                }
            }
            else
            {
                for (int y = yFrom; y <= yTo; y++)
                {
                    for (int x = xFrom; x <= xTo; x++)
                    {
                        if (map.ContainsKey((y, x)))
                        {
                            map[(y, x)] += 2;
                        }
                        else
                        {
                            map[(y, x)] = 2;
                        }
                    }
                }
            }
        }

        return map.Sum(m => m.Value).ToString();
    }
}