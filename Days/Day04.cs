﻿namespace Days
{
    internal class Day04 : BaseDay
    {
        public Day04()
        {
            SampleInputPartOne.Add(@"abcdef", "609043");
            SampleInputPartOne.Add(@"pqrstuv", "1048970");

            SampleInputPartTwo.Add(@"", "");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int solution = FindMagicNumber(input[0], 5);

            return solution.ToString();
        }

        private int FindMagicNumber(string v, int numOfZeros)
        {
            int counter = 0;
            while (true)
            {
                string input = v + counter++.ToString();

                System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
                byte[] hashBytes = md5.ComputeHash(Encoding.UTF8.GetBytes(input));

                var ba = Convert.ToHexString(hashBytes);
                if (ba.TrimStart('0').Length == ba.Length - numOfZeros)
                {
                    return counter - 1;
                }
            }
        }

        protected override string FindSecondSolution(string[] input)
        {
            int solution = FindMagicNumber(input[0], 6);

            return solution.ToString();
        }
    }
}