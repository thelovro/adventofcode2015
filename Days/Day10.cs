﻿namespace Days
{
    internal class Day10 : BaseDay
    {
        public Day10()
        {
            SampleInputPartOne.Add(@"1", "6");

            SampleInputPartTwo.Add(@"", "");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int numberOfTurns = IsTest ? 5 : 40;
            string sequence = input[0];

            sequence = PlayTheGame(numberOfTurns, sequence);

            return sequence.Length.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            int numberOfTurns = IsTest ? 5 : 50;
            string sequence = input[0];
            sequence = PlayTheGame(numberOfTurns, sequence);

            return sequence.Length.ToString();
        }

        private static string PlayTheGame(int numberOfTurns, string sequence)
        {
            for (int i = 0; i < numberOfTurns; i++)
            {
                StringBuilder newSequence = new();
                for (int j = 0; j < sequence.Length; j++)
                {
                    int charCounter = 1;
                    while (j + charCounter < sequence.Length && sequence[j + charCounter] == sequence[j]) charCounter++;

                    newSequence.Append(charCounter);
                    newSequence.Append(sequence[j]);
                    j += charCounter - 1;
                }

                sequence = newSequence.ToString();
            }

            return sequence;
        }
    }
}