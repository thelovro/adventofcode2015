﻿namespace Days;
class Day02 : BaseDay
{
    public Day02()
    {
        SampleInputPartOne.Add(@"2x3x4", "58");
        SampleInputPartOne.Add(@"1x1x10", "43");

        SampleInputPartTwo.Add(@"2x3x4", "34");
        SampleInputPartTwo.Add(@"1x1x10", "14");
    }

    protected override string FindFirstSolution(string[] input)
    {
        List<int> size = new();

        foreach(string gift in input)
        {
            int l = Convert.ToInt32(gift.Split('x')[0]);
            int w = Convert.ToInt32(gift.Split('x')[1]);
            int h = Convert.ToInt32(gift.Split('x')[2]);

            int first = l * w;
            int second = l * h;
            int third = w * h;

            size.Add(2 * first + 2 * second + 2 * third + new List<int>() { first, second, third }.Min());
        }

        return size.Sum().ToString();
    }

    protected override string FindSecondSolution(string[] input)
    {
        List<int> length = new();

        foreach (string gift in input)
        {
            int l = Convert.ToInt32(gift.Split('x')[0]);
            int w = Convert.ToInt32(gift.Split('x')[1]);
            int h = Convert.ToInt32(gift.Split('x')[2]);

            int first = l + w;
            int second = l + h;
            int third = w + h;

            length.Add(2 * new List<int>() { first, second, third }.Min() + l * w * h);
        }

        return length.Sum().ToString();
    }
}