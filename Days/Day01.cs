﻿namespace Days;

class Day01 : BaseDay
{
    public Day01()
    {
        SampleInputPartOne.Add(@"(())", "0");
        SampleInputPartOne.Add(@"(()(()(", "3");
        SampleInputPartOne.Add(@"))(((((", "3");
        SampleInputPartOne.Add(@")())())", "-3");
        SampleInputPartOne.Add(@"))(", "-1");

        SampleInputPartTwo.Add(@")", "1");
        SampleInputPartTwo.Add(@"()())", "5");
    }

    protected override string FindFirstSolution(string[] input)
    {
        var result = input[0].GroupBy(i => i).Select(i => new { i.Key, Count = i.Count() });

        int up = result.First(r => r.Key == '(').Count;
        int down = result.First(r => r.Key == ')').Count;

        return (up - down).ToString();
    }

    protected override string FindSecondSolution(string[] input)
    {
        int level = 0;
        int counter = 0;
        int result = 0;
        foreach(char c in input[0])
        {
            level = c switch
            {
                '(' => level + 1,
                ')' => level - 1,
                _ => throw new Exception("Should not happend!")
            };

            counter++;

            if (level == -1)
            {
                result = counter;
                break;
            }
        }

        return result.ToString();
    }
}