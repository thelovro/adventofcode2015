﻿using System.Text.RegularExpressions;

namespace Days
{
    internal class Day09 : BaseDay
    {
        public Day09()
        {
            SampleInputPartOne.Add(@"London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141", "605");

            SampleInputPartTwo.Add(@"London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141", "982");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int minDistance = int.MaxValue;
            Dictionary<(string, string), int> distances = GetDistances(input);
            HashSet<string> cities = GetCities(input);


            foreach (string city in cities)
            {
                List<string> path = new() { city };
                (int min, int max) = GoTravel(path, cities, distances, 0);

                if (min < minDistance)
                {
                    minDistance = min;
                }
            }

            return minDistance.ToString();
        }

        private (int,int) GoTravel(List<string> path, HashSet<string> cities, Dictionary<(string, string), int> distances, int totalDistance)
        {
            if (path.Count == cities.Count) return (totalDistance, totalDistance);

            int minDistance = int.MaxValue;
            int maxDistance = 0;
            foreach (var nextCity in distances.Where(d => d.Key.Item1 == path[path.Count - 1]))
            {
                if (path.Contains(nextCity.Key.Item2)) continue;

                //duplicate path list
                var newPath = path.GetRange(0, path.Count);
                newPath.Add(nextCity.Key.Item2);

                (int min, int max) = GoTravel(newPath, cities, distances, totalDistance + nextCity.Value);
                if (min < minDistance) 
                    minDistance = min;

                if (max > maxDistance)
                    maxDistance = max;
            }

            return (minDistance, maxDistance);
        }

        private HashSet<string> GetCities(string[] input)
        {
            HashSet<string> cities = new();
            foreach (var line in input)
            {
                string pattern = @"^(\w+) to (\w+) = (\d+)$";
                Regex regex = new Regex(pattern);
                Match match = regex.Match(line);

                cities.Add(match.Groups[1].Value);
                cities.Add(match.Groups[2].Value);
            }
            return cities;
        }

        private Dictionary<(string, string), int> GetDistances(string[] input)
        {
            Dictionary<(string, string), int> distances = new();

            foreach (var line in input)
            {
                string pattern = @"^(\w+) to (\w+) = (\d+)$";
                Regex regex = new Regex(pattern);
                Match match = regex.Match(line);

                distances.Add((match.Groups[1].Value, match.Groups[2].Value), int.Parse(match.Groups[3].Value));
                distances.Add((match.Groups[2].Value, match.Groups[1].Value), int.Parse(match.Groups[3].Value));
            }

            return distances;
        }

        protected override string FindSecondSolution(string[] input)
        {
            int maxDistance = 0;
            Dictionary<(string, string), int> distances = GetDistances(input);
            HashSet<string> cities = GetCities(input);


            foreach (string city in cities)
            {
                List<string> path = new() { city };
                (int min, int max) = GoTravel(path, cities, distances, 0);

                if (max > maxDistance)
                {
                    maxDistance = max;
                }
            }

            return maxDistance.ToString();
        }
    }
}