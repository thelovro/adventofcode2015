﻿using System.Threading;
using System.Windows.Forms;

namespace Days;

public abstract class BaseDay
{
    private const int year = 2015;
    protected bool IsTest = false;

    static Dictionary<string, string>? sampleInputPartOne;
    protected static Dictionary<string, string> SampleInputPartOne
    {
        get
        {
            if (sampleInputPartOne == null)
            {
                sampleInputPartOne = new();
            }

            return sampleInputPartOne;
        }
    }

    static Dictionary<string, string>? sampleInputPartTwo;
    protected static Dictionary<string, string> SampleInputPartTwo
    {
        get
        {
            if (sampleInputPartTwo == null)
            {
                sampleInputPartTwo = new();
            }

            return sampleInputPartTwo;
        }
    }

    public string DayName
    {
        get { return this.GetType().Name; }
    }

    public string[] FinalInput
    {
        get
        {
            if (System.IO.File.Exists($@"..\..\..\{DayName}Input.txt"))
            {
                return System.IO.File.ReadAllLines($@"..\..\..\{DayName}Input.txt");
            }
            else
            {
                return Array.Empty<string>();
            }
        }
    }

    protected abstract string FindFirstSolution(string[] input);

    protected abstract string FindSecondSolution(string[] input);

    public void Solve()
    {
        try
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("******* ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write($"Advent Of Code {year} ({DayName})");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" *******");
            Console.WriteLine();

            #region Part 1
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine();
            Console.WriteLine("Part one");
            Console.WriteLine("===============================================");

            int sampleOneCounter = 1;
            foreach (var input in SampleInputPartOne)
            {
                IsTest = true;
                PrepareOutput(input.Key.Split(Environment.NewLine), FindFirstSolution, sampleOneCounter++, input.Value);
                IsTest = false;
            }

            var firstSolution = PrepareOutput(FinalInput, FindFirstSolution);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("===============================================");
            #endregion Part 2

            #region Part 2
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine();
            Console.WriteLine("Part two");
            Console.WriteLine("===============================================");

            int sampleTwoCounter = 1;
            foreach (var input in SampleInputPartTwo)
            {
                if (input.Key == "") continue;

                IsTest = true;
                PrepareOutput(input.Key.Split(Environment.NewLine), FindSecondSolution, sampleTwoCounter++, input.Value);
                IsTest = false;
            }

            var secondSolution = PrepareOutput(FinalInput, FindSecondSolution);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("===============================================");
            #endregion Part 2

            Console.WriteLine();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("Instructions:");
            Console.WriteLine("- Press '1' to copy first solution to clipboard.");
            Console.WriteLine("- Press '2' to copy second solution to clipboard.");
            Console.WriteLine("- Press '3' to celebrate.");
            Console.WriteLine("- Press any other key to close the command prompt.");

            ConsoleKeyInfo inputString = new ConsoleKeyInfo();
            while (inputString.KeyChar == 0 || inputString.KeyChar.ToString() == "1" || inputString.KeyChar.ToString() == "2" || inputString.KeyChar.ToString() == "3")
            {
                inputString = Console.ReadKey();
                Console.WriteLine();

                if (inputString.KeyChar.ToString() == "1")
                {
                    Clipboard.SetText(firstSolution);
                    Console.WriteLine("First solution copied to clipboard...");
                }
                else if (inputString.KeyChar.ToString() == "2")
                {
                    Clipboard.SetText(secondSolution);
                    Console.WriteLine("Second solution copied to clipboard...");
                }
                else if (inputString.KeyChar.ToString() == "3")
                {
                    var color = Console.ForegroundColor;
                    string text = "Woohoo!! Congratulations for the right solution!!!";

                    for (int i = 0; i < 10; i++)
                    {
                        Console.ForegroundColor = Console.ForegroundColor == ConsoleColor.Green ? ConsoleColor.Red : ConsoleColor.Green;

                        Console.Write(text);

                        Thread.Sleep(200);
                        Console.CursorLeft = 0;
                    }

                    Console.WriteLine();

                    Console.ForegroundColor = color;
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"There was an error while solving AoC: {ex.ToString()}!");
        }
    }

    public string PrepareOutput(string[] input, Func<string[], string> Function, int? sequence = null, string? expectedSolution = null)
    {
        Stopwatch watch = new();
        watch.Start();
        string solution = Function(input);
        watch.Stop();

        if (solution == string.Empty)
            return "";

        bool isSample = expectedSolution != null;

        Console.ForegroundColor = ConsoleColor.DarkGreen;
        Console.Write(isSample ? $"{sequence}: " : $"Final: ");

        Console.ForegroundColor = ConsoleColor.DarkYellow;
        Console.ForegroundColor = !isSample ? ConsoleColor.Cyan : (solution == expectedSolution ? ConsoleColor.Green : ConsoleColor.Red);
        Console.Write($"{solution}");
        if (isSample && solution != expectedSolution)
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.Write($" (expected: {expectedSolution})");
        }

        Console.ForegroundColor = ConsoleColor.DarkYellow;
        Console.WriteLine($" ({watch.Elapsed.TotalMilliseconds} ms)");

        return solution;
    }
}