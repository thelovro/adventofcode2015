﻿namespace Days
{
    internal class Day11 : BaseDay
    {
        public Day11()
        {
            SampleInputPartOne.Add(@"abcdefgh", "abcdffaa");
            SampleInputPartOne.Add(@"ghijklmn", "ghjaabcc");

            SampleInputPartTwo.Add(@"", "");
        }

        protected override string FindFirstSolution(string[] input)
        {
            string newPassword = CalculateNextPassword(input[0]);

            return newPassword;
        }

        private string CalculateNextPassword(string password)
        {
            List<char> passwordChars = password.ToList();
            while (true)
            {
                int index = password.Length - 1;
                passwordChars[index]++;
                while(passwordChars[index] == 123)
                {
                    passwordChars[index] = 'a';
                    index--;
                    passwordChars[index]++;
                }

                //check rule number one
                bool sequenceFound = false;
                for (int i = 0; i < passwordChars.Count-2; i++)
                {
                    if (passwordChars[i] + 1 == passwordChars[i + 1] && passwordChars[i] + 2 == passwordChars[i + 2]) {
                        sequenceFound = true; 
                        break; 
                    }
                }
                if (!sequenceFound) continue;

                //check rule number two
                if (passwordChars.Contains('i') || passwordChars.Contains('o') || passwordChars.Contains('l')) continue;

                //check rule number three
                List<char> pairs = new();
                for (int i = 0; i < passwordChars.Count - 1; i++)
                {
                    if (passwordChars[i] == passwordChars[i + 1] && !pairs.Contains(passwordChars[i]))
                    {
                        pairs.Add(passwordChars[i]);
                    }
                }
                if (pairs.Count < 2) continue;

                //if every rule is OK, exit while loop
                break;
            }

            return string.Join("", passwordChars);
        }

        protected override string FindSecondSolution(string[] input)
        {
            string newPassword = CalculateNextPassword(input[0]);
            newPassword = CalculateNextPassword(newPassword);

            return newPassword;
        }
    }
}