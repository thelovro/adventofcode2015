﻿using System.Text.RegularExpressions;

namespace Days
{
    internal class Day15 : BaseDay
    {
        public Day15()
        {
            SampleInputPartOne.Add(@"Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3", "62842880");

            SampleInputPartTwo.Add(@"Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3", "57600000");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int numberOfTeaspoons = 100;
            Dictionary<string, (int capacity, int durability, int flavor, int texture, int calories)> ingredients = ParseIngredients(input);

            int maxValue = StartAddingIngredients(new(), ingredients, numberOfTeaspoons);

            return maxValue.ToString();
        }

        private int StartAddingIngredients(Dictionary<string, int> alreadyUsedIngredients, 
            Dictionary<string, (int capacity, int durability, int flavor, int texture, int calories)> ingredients, 
            int numberOfTeaspoons,
            int? calorieCount = null)
        {
            int max = 0;
            foreach (var ingredient in ingredients)
            {
                if (alreadyUsedIngredients.ContainsKey(ingredient.Key)) continue;

                //add last ingredient
                if (alreadyUsedIngredients.Count == ingredients.Count - 1)
                {
                    alreadyUsedIngredients.Add(ingredient.Key, numberOfTeaspoons - alreadyUsedIngredients.Values.Sum());

                    int value = CalculateValue(alreadyUsedIngredients, ingredients, calorieCount);
                    if (value > max) max = value;
                }
                else
                {
                    //add next ingredient
                    for (int i = 0; i <= numberOfTeaspoons; i++)
                    {
                        Dictionary<string, int> newAlreadyUsedIngredients = alreadyUsedIngredients.ToDictionary();
                        newAlreadyUsedIngredients.Add(ingredient.Key, i);

                        int value = StartAddingIngredients(newAlreadyUsedIngredients, ingredients, numberOfTeaspoons, calorieCount);
                        if (value > max) max = value;
                    }
                }
                break;
            }
         
            return max;
        }

        private int CalculateValue(Dictionary<string, int> alreadyUsedIngredients, 
            Dictionary<string, (int capacity, int durability, int flavor, int texture, int calories)> ingredients, 
            int? calorieCount)
        {
            int totalCapacity = 0;
            int totalDurability = 0;
            int totalFlavor = 0;
            int totalTexture = 0;
            int totalCalories = 0;

            foreach (var ingredient in alreadyUsedIngredients)
            {
                totalCapacity += ingredient.Value * ingredients[ingredient.Key].capacity;
                totalDurability += ingredient.Value * ingredients[ingredient.Key].durability;
                totalFlavor += ingredient.Value * ingredients[ingredient.Key].flavor;
                totalTexture += ingredient.Value * ingredients[ingredient.Key].texture;
                totalCalories += ingredient.Value * ingredients[ingredient.Key].calories;
            }

            if (totalCapacity < 0 || totalDurability < 0 || totalFlavor < 0 || totalTexture < 0) return 0;
            if (calorieCount.HasValue && totalCalories != calorieCount.Value) return 0; 

            return totalCapacity * totalDurability * totalFlavor * totalTexture;
        }

        private Dictionary<string, (int capacity, int durability, int flavor, int texture, int calories)> ParseIngredients(string[] input)
        {
            Dictionary<string, (int capacity, int durability, int flavor, int texture, int calories)> ingredients = new();

            Regex regex = new Regex("(?<Name>.+): capacity (?<Capacity>-?\\d+), durability (?<Durability>-?\\d+), flavor (?<Flavor>-?\\d+), texture (?<Texture>-?\\d+), calories (?<Calories>\\d+)");
            foreach (var line in input)
            {
                var match = regex.Match(line);
                ingredients.Add(match.Groups["Name"].Value, (
                    int.Parse(match.Groups["Capacity"].Value), 
                    int.Parse(match.Groups["Durability"].Value), 
                    int.Parse(match.Groups["Flavor"].Value), 
                    int.Parse(match.Groups["Texture"].Value), 
                    int.Parse(match.Groups["Calories"].Value)));
            }

            return ingredients;
        }

        protected override string FindSecondSolution(string[] input)
        {
            int numberOfTeaspoons = 100;
            int calorieCount = 500;
            Dictionary<string, (int capacity, int durability, int flavor, int texture, int calories)> ingredients = ParseIngredients(input);

            int maxValue = StartAddingIngredients(new(), ingredients, numberOfTeaspoons, calorieCount);

            return maxValue.ToString();
        }
    }
}