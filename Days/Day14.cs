﻿using System.Text.RegularExpressions;

namespace Days
{
    internal class Day14 : BaseDay
    {
        public Day14()
        {
            SampleInputPartOne.Add(@"Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.", "1120");

            SampleInputPartTwo.Add(@"Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.", "689");
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<(string name, int speed, int secondsMove, int secondsRest)> reindeers = ParseReindeerData(input);

            int numberOfSeconds = IsTest ? 1000 : 2503;
            int maxTravelDistance = GetMaxTravelDistance(reindeers, numberOfSeconds);

            return maxTravelDistance.ToString();
        }

        private int GetMaxTravelDistance(List<(string name, int speed, int secondsMove, int secondsRest)> reindeers, int numberOfSeconds)
        {
            int maxTravelDistance = 0;

            foreach (var reindeer in reindeers)
            {
                int numberOfCycles = numberOfSeconds / (reindeer.secondsMove + reindeer.secondsRest);
                int remainder = numberOfSeconds % (reindeer.secondsMove + reindeer.secondsRest);

                int reindeerDistance = numberOfCycles * reindeer.speed * reindeer.secondsMove + Math.Min(remainder, reindeer.secondsMove) * reindeer.speed;
                if (reindeerDistance > maxTravelDistance)
                {
                    maxTravelDistance = reindeerDistance;
                }
            }

            return maxTravelDistance;
        }

        private List<(string name, int speed, int secondsMove, int secondsRest)> ParseReindeerData(string[] input)
        {
            List<(string name, int speed, int secondsMove, int secondsRest)> reindeers = new();
            
            Regex regex = new Regex("(?<Name>.+) can fly (?<Speed>\\d+) km.s for (?<TimeMovement>\\d+) seconds, but then must rest for (?<TimeRest>\\d+) seconds.");
            foreach(var line in input)
            {
                var match = regex.Match(line);
                reindeers.Add((match.Groups["Name"].Value, int.Parse(match.Groups["Speed"].Value), int.Parse(match.Groups["TimeMovement"].Value), int.Parse(match.Groups["TimeRest"].Value)));
            }

            return reindeers;
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<(string name, int speed, int secondsMove, int secondsRest)> reindeers = ParseReindeerData(input);

            int numberOfSeconds = IsTest ? 1000 : 2503;
            int maxPoints = GetMaxPoints(reindeers, numberOfSeconds);

            return maxPoints.ToString();
        }

        private int GetMaxPoints(List<(string name, int speed, int secondsMove, int secondsRest)> reindeers, int numberOfSeconds)
        {
            Dictionary<string, int> reindeerDistance = reindeers.ToDictionary(r => r.name, r=> 0);
            Dictionary<string, int> reindeerPoints = reindeers.ToDictionary(r => r.name, r => 0);
            
            for (int i = 0; i < numberOfSeconds; i++)
            {
                foreach (var reindeer in reindeers)
                {
                    int remainder = i % (reindeer.secondsMove + reindeer.secondsRest);

                    if (remainder >= reindeer.secondsMove) continue;

                    reindeerDistance[reindeer.name] += reindeer.speed;
                }

                int maxDistance = reindeerDistance.Max(r=>r.Value);
                foreach (var reindeer in reindeerDistance.Where(r=>r.Value == maxDistance))
                {
                    reindeerPoints[reindeer.Key]++;
                }
            }

            return reindeerPoints.Max(r=>r.Value);
        }
    }
}