﻿namespace Days
{
    internal class Day08 : BaseDay
    {
        public Day08()
        {
            SampleInputPartOne.Add(@"""""
""abc""
""aaa\""aaa""
""\x27""", "12");

            SampleInputPartTwo.Add(@"""""
""abc""
""aaa\""aaa""
""\x27""", "19");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int sumCode = 0;
            int sumChars = 0;
            foreach (var line in input)
            {
                sumCode += line.Length;

                var unescaped = System.Text.RegularExpressions.Regex.Unescape(line);
                sumChars += unescaped.Length - 2;
            }
            return (sumCode - sumChars).ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            int sumCode = 0;
            int sumChars = 0;
            foreach (var line in input)
            {
                sumCode += line.Length;

                var escaped = line.Replace("\\", "\\\\").Replace("\"", "\\\"");
                sumChars += escaped.Length + 2;
            }
            return (sumChars - sumCode).ToString();
        }
    }
}