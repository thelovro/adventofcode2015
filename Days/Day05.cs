﻿namespace Days;

internal class Day05 : BaseDay
{
    public Day05()
    {
        SampleInputPartOne.Add(@"ugknbfddgicrmopn", "1");
        SampleInputPartOne.Add(@"aaa", "1");
        SampleInputPartOne.Add(@"jchzalrnumimnmhp", "0");
        SampleInputPartOne.Add(@"haegwjzuvuyypxyu", "0");
        SampleInputPartOne.Add(@"dvszwmarrgswjxmb", "0");

        SampleInputPartTwo.Add(@"xyxy", "1");
        SampleInputPartTwo.Add(@"aabcdefgaa", "0");
        SampleInputPartTwo.Add(@"aaa", "0");
        SampleInputPartTwo.Add(@"qjhvhtzxzqqjkmpb", "1");
        SampleInputPartTwo.Add(@"xxyxx", "1");
        SampleInputPartTwo.Add(@"uurcxstgmygtbstg", "0");
        SampleInputPartTwo.Add(@"ieodomkazucvgmuy", "0");
    }

    protected override string FindFirstSolution(string[] input)
    {
        long niceCounter = 0;
        foreach (string line in input)
        {
            if (CheckLine(line)) niceCounter++;
        }

        return niceCounter.ToString();
    }

    private bool CheckLine(string line)
    {
        List<string> badStrings = new() { "ab", "cd", "pq", "xy" };
        List<char> vowels = new() { 'a', 'e', 'i', 'o', 'u' };

        //without bad strings
        foreach (var item in badStrings)
        {
            if (line.Contains(item)) return false;
        }

        //at least three vowels
        int vowelCounter = 0;
        foreach (char c in line)
        {
            if (vowels.Contains(c)) vowelCounter++;
        }
        if (vowelCounter < 3) return false;

        //at least one letter twice in a row
        for (int i = 0; i < line.Length; i++)
        {
            if (i < line.Length - 1 && line[i] == line[i + 1])
                return true;
        }

        return false;
    }

    protected override string FindSecondSolution(string[] input)
    {
        long niceCounter = 0;
        foreach (string line in input)
        {
            if (CheckLinePartTwo(line)) niceCounter++;
        }

        return niceCounter.ToString();
    }

    private bool CheckLinePartTwo(string line)
    {
        //check for double letter pairs
        bool hasDoubleLetterPair = false;
        for (int i = 0; i < line.Length; i++)
        {
            if (i == line.Length - 1) return false;

            string word = $"{line[i]}{line[i + 1]}";
            for (int j = i + 2; j < line.Length; j++)
            {
                if (j == line.Length - 1) break;

                if (word == $"{line[j]}{line[j + 1]}")
                {
                    hasDoubleLetterPair = true;
                    break;
                }
            }
            if (hasDoubleLetterPair) break;
        }
        if (!hasDoubleLetterPair) return false;

        //check for repeating letter with one between
        for (int i = 0; i < line.Length; i++)
        {
            if (i == line.Length - 2) return false;

            if (line[i] == line[i + 2]) return true;
        }
        return false;
    }
}